var script_area = null;

var nodes = [];
var edges = [];
var g_type = null;
var edge = [];
var last_selected = null;
var start_select = null;

var SCALE_WIDTH = 1;
var SCALE_HEIGTH = 1;
var SCALE_STROKE = 1;

var grid_dots = [];

var grid = [];



async function importa() {


  //test();

  //
  // $.getScript( "RiegerNishimuraLattice.js", function( data, textStatus, jqxhr ) {
  //   graph.log( "data" ); // Data returned
  //   console.log( textStatus ); // Success
  //   console.log( jqxhr.status ); // 200
  //   console.log( "Load was performed." );
  // });

//  graph.log("OXE");

}

/// Console

// define a new console
var graph = (function(oldCons){
    return {
        log: function(text){
            document.getElementById("output").value = document.getElementById("output").value + text;
        },
        info: function (text) {
            document.getElementById("errors").value = document.getElementById("errors").value + text;
        },
        warn: function (text) {
            document.getElementById("errors").value = document.getElementById("errors").value + text;
        },
        error: function (text) {
            document.getElementById("errors").value = document.getElementById("errors").value + text;
        }
    };
}(window.console));

window.console = console;


////

KEY_COMMAND = false;

function setKeyboardEvents() {


  document.addEventListener('keydown', (e) => {
    if (e.code === "ShiftLeft") KEY_COMMAND = true;

});

  document.addEventListener('keyup', (e) => {

      if (KEY_COMMAND) {
        if (e.code === "KeyV") switchType_node();
        else if (e.code === "KeyA") switchType_edge();
        else if (e.code === "KeyS") select();
        else if (e.code === "KeyG") gen();
        else if (e.code === "KeyI") ideal();
        else if (e.code === "KeyD") doDelete();
        else if (e.code === "KeyR") reset();
        else if (e.code === "KeyT") getTopo();
        else if (e.code === "Equal" || e.code === "NumpadAdd") zoom_in();
        else if (e.code === "Minus" || e.code === "NumpadSubtract") zoom_out();
        else if (e.code === "KeyC") openTab("Script");
      }
      if (e.code === "ShiftLeft")
        KEY_COMMAND = false;
      else if (e.code === "Escape") closeTab();

  });

}


function startCodeMirror() {

  if (script_area == null) {
  script_area = CodeMirror.fromTextArea(document.getElementById("script"), {
    mode:  "javascript",
    lineNumbers: true,
    theme: "blackboard",
    extraKeys: {"Ctrl-Space": "autocomplete"},
    styleActiveLine: true,
    lint: true,
    gutters: ["CodeMirror-lint-markers"],
  });

  // restore backup
  var savedText = localStorage.getItem("script") || "";
  script_area.setValue(savedText);

  }


}

function startBoard() {
    board.start();

    setKeyboardEvents();

    manageGrid(document.getElementById("grid"));

	//	document.getElementById("output").value= "graph.io output.";
    // CodeMirror.commands.autocomplete = function(cm) {
    //   cm.showHint({hint: CodeMirror.hint.anyword});
    // }


   //document.getElementById("footer").style.maxWidth =  window.screen.width  + "px";
   //document.getElementById("board").style.maxWidth =  window.screen.width - 50 + "px";
   //document.getElementById("board").style.maxHeight =  window.screen.height - 50 + "px";

   window.addEventListener('resize', function() {

      //document.getElementById("footer").style.maxWidth =  window.screen.width + "px";
    //  document.getElementById("board").style.maxWidth =  window.screen.width - 50 + "px";

   });

// save backup
   window.addEventListener("change", () => {
     var text = script_area.getValue();
     localStorage.setItem("script", text);
     console.log("aeasda");
    });



    if (document.getElementById("grid").checked) {
      makeGrid();
    }

    document.getElementById("output").value = ""
    document.getElementById("errors").value = ""

    window.onerror = function(message, source, lineno, colno, error) {
      document.getElementById("errors").value = source  + "\n ----- \n" + message;
    }

}

function runScript() {

  document.getElementById("output").value = "";
  document.getElementById("errors").value = "";

  fun = new Function(script_area.getValue());

  //script = content;

  //console.log(script_area.getValue())

  board.script(fun);
}

function createComponent(x, y, type) {
//	node = new component(15, 15, "#c82124", x, y, type);
	node = new component(10, 10, "#c82124", x, y, type);
	node.color = "#777777"
	node.color_selected = "#AAAAAA"
	nodes.push(node);
}

function selectAllNodesFromArea(end_pos) {

  for (var i = 0; i < nodes.length; i++) {
    console.log(nodes[i])
    if ( (nodes[i].x > start_select[0] && nodes[i].x < end_pos[0]) &&
         (nodes[i].y > start_select[1] && nodes[i].y < end_pos[1]) ) {
      nodes[i].selected = true;
    }
    else if ( (nodes[i].x < start_select[0] && nodes[i].x > end_pos[0]) &&
              (nodes[i].y < start_select[1] && nodes[i].y > end_pos[1]) ) {
          nodes[i].selected = true;
      }
    else if ( (nodes[i].x < start_select[0] && nodes[i].x > end_pos[0]) &&
              (nodes[i].y > start_select[1] && nodes[i].y < end_pos[1]) ) {
            nodes[i].selected = true;
    }
    else if ( (nodes[i].x > start_select[0] && nodes[i].x < end_pos[0]) &&
              (nodes[i].y < start_select[1] && nodes[i].y > end_pos[1]) ) {
            nodes[i].selected = true;
    }
  }

}

function canvas_arrow(context, fromx, fromy, tox, toy, event) {
  var headlen = 10 * SCALE_STROKE; // length of head in pixels
  var dx = tox - fromx;
  var dy = toy - fromy;
  var angle = Math.atan2(dy, dx);
  if (event != "mousemove") {
    tox = tox - (headlen * Math.cos(angle));
    toy = toy - (headlen * Math.sin(angle));
  }
  context.moveTo(fromx, fromy);
  context.lineTo(tox, toy);
  context.moveTo(tox, toy);
  context.lineTo(tox - headlen * Math.cos(angle - Math.PI / 6), toy - headlen * Math.sin(angle - Math.PI / 6));
  context.moveTo(tox, toy);
  context.lineTo(tox - headlen * Math.cos(angle + Math.PI / 6), toy - headlen * Math.sin(angle + Math.PI / 6));
  //console.log(dx, dy, angle, tox, toy);
}


function manageGrid(checkbox) {
    if(checkbox.checked == true){
        makeGrid();
    }else{
        context = board.grid;
        context.clearRect(0, 0, this.background.width, this.background.height);
        context.closePath();
   }
}

function makeGrid() {
  var grid_x = [], grid_y = [];

  context = board.grid;
  context.lineWidth = 0.1;

  var count = 0 ;

  for (var i = 10; i < context.canvas.width; i=i+50) {
    context.moveTo (i, 0);
    context.lineTo(i, context.canvas.height );

    grid_x.push(i);
  }

  for (var i = 10; i < context.canvas.height; i=i+50) {
    context.moveTo (0, i);
    context.lineTo( context.canvas.width, i );

    grid_y.push(i);
  }

  for (var i = 0; i < grid_x.length; i++) {
    grid.push([[]]);
    for (var j = 0; j < grid_y.length; j++) {
        grid_dots.push([grid_x[i], grid_y[j]]);
        grid[i][j] = [grid_x[i], grid_y[j]];
    }
  }

  context.stroke();

  console.log(grid_dots)


}

// receive a component and return the nearest point of it
// via Euclidean Distance
function getNearBy(component) {
  var nearby = grid_dots[0];

  for (var i = 1; i < grid_dots.length; i++) {
    dist = Math.sqrt(Math.pow((component.x - grid_dots[i][0]), 2) + Math.pow((component.y - grid_dots[i][1]), 2));
    nearby_dist = Math.sqrt(Math.pow((component.x - nearby[0]), 2) + Math.pow((component.y - nearby[1]), 2));

    if (dist < nearby_dist)
    {
       nearby = grid_dots[i];
     }

  }

  return nearby;
}

function drawBoard(pos) {
	board.clear();
	board.context.beginPath();
  board.context.lineWidth = 2 * SCALE_STROKE;


  if (g_type == "select_rect") {

    //board.context.beginPath();
    board.context.rect(start_select[0], start_select[1], pos[0] - start_select[0], pos[1] - start_select[1]);
    board.context.stroke();

  }

	if (g_type == "edge") {
		for (var i = 0; i < nodes.length; i++) {
				if (nodes[i].selected) {
          canvas_arrow (board.context, nodes[i].x, nodes[i].y, pos[0], pos[1], "mousemove");
				//	board.context.moveTo(nodes[i].x, nodes[i].y);
				//	board.context.lineTo(pos[0], pos[1]);
					board.context.stroke();
					//console.log(pos[0], pos[1]);
				}
			}
	}

  for (var i = 0; i < edges.length; i++) {
    canvas_arrow (board.context, edges[i][0].x, edges[i][0].y,  edges[i][1].x, edges[i][1].y, "mouseup");
    board.context.stroke();
  }
  //
	// for (var i = 0; i < nodes.length; i++) {
	// 	for (var j = 0; j < edges.length; j++) {
  //
  //
	// 		for (var k = 0; k < edges[j].length; k++) {
	// 			if (nodes[i] == edges[j][k]) {
  //           canvas_arrow (board.context, nodes[i].x, nodes[i].y, edges[j][((k + 1) % 2)].x, edges[j][((k + 1) % 2)].y);
	// 					//board.context.moveTo(nodes[i].x, nodes[i].y);
	// 				//	board.context.lineTo(edges[j][((k + 1) % 2)].x, edges[j][((k + 1) % 2)].y);
	// 					board.context.stroke();
  //         //  board.context.lineCap = 'round';
	// 			}
	// 		}
	// 	}
	// }

	for (var i = 0; i < nodes.length; i++) {
		nodes[i].path = new Path2D();

    if (document.getElementById("fit").checked) {

      nearby = getNearBy(nodes[i]);
      nodes[i].x = nearby[0];
      nodes[i].y = nearby[1];

    }


    board.context.moveTo(nodes[i].x, nodes[i].y);
		//board.context.fillStyle = nodes[i].color;
		board.draw(nodes[i]);
	}
}

var board = {
    canvas : document.getElementById("board"),
    background : document.getElementById("background"),
    start : function() {
        board.background.style.maxWidth = window.screen.width - 100;
        board.background.width = window.screen.width;
        board.background.height = window.screen.height;
        board.canvas.maxWidth = board.background.style.maxWidth;
        board.canvas.width = board.background.width;
        board.canvas.height = board.background.height;
        this.context = this.canvas.getContext("2d");
        this.grid = this.background.getContext("2d");;
      //  document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        //this.frameNo = 0;
				this.canvas.addEventListener('mousemove', function(e) {
						//setInterval(move, 10);
					pos = getCursorPosition(board.canvas, e);


          if (g_type == "select_rect") {

            drawBoard(pos);



          }

					if (g_type == "edge") {

            // for (var i = 0; i < nodes.length; i++) {
            //   if (board.context.isPointInPath(nodes[i].path, pos[0], pos[1])) {
            //     nodes[i].selected = true;
            //   }
            // }

						drawBoard(pos);
					}
					else if (g_type == "node" || g_type == null) {
            const factor = 50;
            const grid = document.getElementById("fit");

						for (var i = 0; i < nodes.length; i++) {

              if (nodes[i].selected) {
                if (grid.checked) {
                  if ((pos[0] >= (nodes[i].x + factor))) {
								      nodes[i].x = (nodes[i].x + factor);
								      drawBoard(pos);
							   }
                 else if (nodes[i].selected && (pos[0] <= (nodes[i].x - factor))) {
								     nodes[i].x = (nodes[i].x - factor);
								     drawBoard(pos);
							    }
                  else if (nodes[i].selected && (pos[1] >= (nodes[i].y + factor))) {
								     nodes[i].y = (nodes[i].y + factor);
								     drawBoard(pos);
							     }
                  else if (nodes[i].selected && (pos[1] <= (nodes[i].y - factor))) {
								     nodes[i].y = (nodes[i].y - factor);
								     drawBoard(pos);
							    }
						    }
                else {

                  if (nodes[i].selected) {
                    nodes[i].x = pos[0];
                    nodes[i].y = pos[1];
                    drawBoard(pos);
                  }
                }
              }
            }
          }
				});
				this.canvas.addEventListener('mousedown', function(e) {
					pos = getCursorPosition(board.canvas, e);
          select_none = true;

					for (var i = 0; i < nodes.length; i++) {
						if (board.context.isPointInPath(nodes[i].path, pos[0], pos[1])) {
              select_none = false;
							if (g_type == "combine") {
								//nodes[i].selected = true;
                //edge = [];
                //edge.push (nodes[i]);
								console.log("combine mousedown");
							}
							else if (g_type == null || g_type == "node") {
								//board.context.clearRect(nodes[i].x-nodes[i].width, nodes[i].y-nodes[i].height,
									//												2 * nodes[i].width, 2 * nodes[i].height);
								nodes[i].selected = true;
							}
							else if (g_type == "edge") {
								nodes[i].selected = true;
								edge = [];
								edge.push (nodes[i]);
							}
              else if (g_type == "select") {
                last_selected = nodes[i];
								nodes[i].selected = !nodes[i].selected;
							}
						}
					}

          if (select_none) {
            if (g_type == "select") {
              g_type = "select_rect"
              start_select = pos;
              console.log("oxe")
            }
          }

					drawBoard(pos);
				});
				this.canvas.addEventListener('mouseup', function(e) {
					var select = false;
					pos = getCursorPosition(board.canvas, e);

          if (g_type == "select_rect") {

            selectAllNodesFromArea(pos);
            g_type = "select";
            drawBoard(pos);
          }


          if (g_type == "delete") {
            var t_edges = [];

            for (var i = 0; i < nodes.length; i++) {
              if (board.context.isPointInPath(nodes[i].path, pos[0], pos[1])) {

                  for (k = 0; k < edges.length; k++) {
                    if (!(edges[k][0] == nodes[i] || edges[k][1] == nodes[i])) {
                      t_edges.push(edges[k]);
                    }
                    // }
                  }


                  edges = t_edges;
                  nodes.splice(i, 1);
                }

              }



              drawBoard();
          }


					if (g_type == "combine") {

						board.add(pos);

						for (var i = 0; i < nodes.length; i++) {

							if (nodes[i].selected) {
								edges.push ([nodes[i], nodes[nodes.length-1]]);
								nodes[i].selected = false;
							}

						}

						drawBoard();


					}

					if (g_type == null || g_type == "node") {
						for (var i = 0; i < nodes.length; i++) {
							if (nodes[i].selected) {
								select = true;
								//nodes[i].x = pos[0];
								//nodes[i].y = pos[1];
								nodes[i].selected = false;
								drawBoard(pos);
							}
					}

					if (!select) {
						board.add(pos);
					  drawBoard(pos);
					}
				}
				else if (g_type == "edge"){

          // turn all off
          for (i = 0; i < nodes.length; i++) {
              nodes[i].selected = false;
              drawBoard(pos);
          }

					for (var i = 0; i < nodes.length; i++) {
							if (board.context.isPointInPath(nodes[i].path, pos[0], pos[1]) && nodes[i] != edge[0]) {
									edge.push(nodes[i]);
									edges.push (edge);
									drawBoard(pos);
							}
						}


				}

			});
    },
    stop : function() {
        clearInterval(this.interval);
    },
    clear : function() {
        board.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
				board.context.closePath();
			//	console.log(this.canvas.height);
    },
		add : function (pos) {
			//	console.log(nodes)
				createComponent(pos[0], pos[1], g_type);
		},
    script : function (fun) {
      fun();
    },
		draw : function (obj) {
				ctx = board.context;

				ctx.beginPath();
				obj.path.arc(obj.x, obj.y, obj.width * SCALE_WIDTH, obj.height * SCALE_HEIGTH, 10 * Math.PI * SCALE_WIDTH);
				ctx.closePath();
				ctx.stroke(obj.path);
        ctx.fillStyle = "gray";

        if (!obj.selected) ctx.fillStyle = obj.color;
        else ctx.fillStyle = obj.color_selected;

        ctx.fill(obj.path);

				//ctx = board.context;
				//obj.path.arc(obj.x, obj.y, obj.width, obj.height, 10 * Math.PI);
				//ctx.fillStyle = obj.color;
				//ctx.fill(obj.path);
		}
}

function component(width, height, color, x, y, type) {
	if (type == "node" || type == null) {
    	this.path = new Path2D();
    	this.type = type;
    	this.width = width;
    	this.height = height;
    	this.speed = 1;
    	this.angle = 0;
    	this.x = x;
    	this.y = y;
			this.selected = false;
    	this.update = function(x, y) {
      //   	ctx = board.context;
      //   	ctx.save();
      //   	ctx.translate(this.x, this.y);
      //   	ctx.rotate(this.angle);
      //   	ctx.fillStyle = color;
		  // 		circle.arc(this.width / -2, this.height / -2, this.width, 0, 10 * Math.PI);
			// 		ctx.fill(circle);
      //   	ctx.restore();
			// 	//ctx.fill();
    	 }
    	this.newPos = function() {
        	this.x += this.speed * Math.sin(this.angle);
        	this.y -= this.speed * Math.cos(this.angle);
    	}
		}
}

// retorna null se não houver base
function detectBase() {
  base = null;
  is_base = true;

  for (var i = 0; i < edges.length; i++) {
    current = edges[i][0];
    for (var j = 0; j < edges.length; j++) {
      if (current == edges[j][1]) {
        is_base = false;
      }
    }
    if (is_base) {
      base = current;
      break;
    }
  }

  return base;
}

function getSingletons (base) {
  var singletons = [];
  for (var i = 0; i < edges.length; i++) {
    if (edges[i][0] == base) {
      singletons.push(edges[i][1]);
      //console.log(edges[i][1])
    }
  }
  return singletons;
}

function uniao(setA, setB) {
    var _uniao = new Set(setA);
    for (var elem of setB) {
        _uniao.add(elem);
    }
    return _uniao;
}

function isOn(topology, open) {

  for (var j = 0; j < topology.length; j++) {
    array = topology[j].sort();
    if (JSON.stringify(array) == JSON.stringify(open.sort())) return true;
  }

  return false;

}

function doExport() {
  document.getElementById("output").value = JSON.stringify(edges);
  openTab("Log");
}

////// Algoritmo em 3 passos para tradução POSET-TOSET  //
// (1) Detecta base.
// (2) Detecta singletons.
// (3) Constrói demais.
////// ************************************************ //
function getTopo()
{
  var topology = new Set();
  const base = detectBase();
  const singletons = getSingletons (base);
  var open_sets = nodes;

  console.log(base)

  for (var i = 0; i < open_sets.length; i++)
    open_sets[i].index = i*10;

  if (base != null) {
    // caso base
    topology.add ([]);

    // hipótese de indução
    for (var k = 0; k < singletons.length; k++) {
        value = k + 1;
        topology.add ([value]);
    }

    for (var i = 0; i < open_sets.length; i++) {
      if (open_sets[i] == base) {
          open_sets[i].index = 0;
          open_sets[i].type = "base";
      }
      for (var j = 0; j < singletons.length; j++) {
        if (open_sets[i] == singletons[j]) {
            open_sets[i].index = j+1;
            open_sets[i].type = "singleton";
        }
      }
    }

    open_sets.sort(function(a, b){return a.y - b.y})
    console.log("AE, ", open_sets);
    // passo indutivo
    var t_index = singletons.length + 1;
    for (var j = 0; j < open_sets.length; j++) {
      if (open_sets[j].type != "base" && open_sets[j].type != "singleton") {
        var open = new Set();
        for (var k = 0; k < edges.length; k++) {
          if (edges[k][1] == open_sets[j]) {
            _index = open_sets.indexOf(edges[k][1]);;
            open_sets[_index].index = t_index;
            index  = open_sets.indexOf(edges[k][0]);
            index = open_sets[index].index;

          //  console.log(index, k, edges[k][0])

            open = uniao(new Set(Array.from(topology)[index]), new Set(open))
          }
        }
        //topo_ = Array.from(topology);
        //console.log("AEU  ", tmp)
        if (!isOn(Array.from(topology), Array.from(open))) {
          topology.add (Array.from(open).sort());
        //  console.log(open)
        //  console.log("nao tem")
        }
        else {
          new_open = open;
          new_element = Array.from(new_open).length;
          while (isOn(Array.from(topology), Array.from(new_open))) {
            new_open = open;
            new_element = new_element + 1;
            new_open = uniao(new Set([new_element]), new Set(new_open));
          }
          topology.add (Array.from(new_open).sort());
          console.log("tem!!!")
        }
        t_index = t_index + 1;
      //  console.log(open)
      }
    }

  //  console.log(topology)
    document.getElementById("output").value = JSON.stringify(Array.from(topology).sort(function(a, b){return a.length - b.length}));

    openTab("Log");
  }
  else {
    alert("Current graph cannot be converted into a topology. Check the structure.");
  }
}

function zoom_in() {
	//board.context.scale(1.2,1.2);
  SCALE_WIDTH = SCALE_WIDTH + 0.25;
  SCALE_HEIGTH = SCALE_HEIGTH + 0.25;
  SCALE_STROKE = SCALE_STROKE + 0.25;
  drawBoard();
}

function zoom_out() {
  //board.context.scale(0.7,0.7);
  SCALE_WIDTH = SCALE_WIDTH - 0.25;
  SCALE_HEIGTH = SCALE_HEIGTH - 0.25;
  SCALE_STROKE = SCALE_STROKE - 0.25;
  drawBoard();
}

function switchType_edge() {
	g_type = "edge";
}

function switchType_node() {
	g_type = "node";
}

function reset() {
	nodes = [];
	edges = [];
	board.clear();
}

function select() {
	g_type = "select";
}

function combine() {
	g_type = "combine";
}

function doDelete() {
	g_type = "delete";
}


function gen() {

  graph = [];

  graph.push(last_selected);

  for (var i = 0; i < nodes.length; i++) {

    if (nodes[i].selected) {
      nodes[i].selected = false;
      graph.push(nodes[i]);
    }

  }

  for (var i = 0; i < graph.length; i++) {
    if (graph[0] != graph[i]) edges.push ([graph[0], graph[i]]);
  }


  drawBoard();



}

function ideal() {

  graph = [];

  graph.push(last_selected);

  for (var i = 0; i < nodes.length; i++) {

    if (nodes[i].selected) {
      nodes[i].selected = false;
      graph.push(nodes[i]);
    }

  }

  for (var i = 0; i < graph.length; i++) {
    if (graph[0] != graph[i]) edges.push ([graph[i], graph[0]]);
  }


  drawBoard();


}

function getCursorPosition(canvas, event) {
    const rect = board.canvas.getBoundingClientRect()
    const x = event.clientX - rect.left
    const y = event.clientY - rect.top
		return [x, y];
}

function openTab(tabName) {
  var i;
  var x = document.getElementsByClassName("tab");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  document.getElementById(tabName).style.display = "block";

  console.log("auuauuaa");
  if (tabName == "Script") {
    startCodeMirror();
  }

  document.getElementById("stage").style.height = (document.getElementById('stage').offsetHeight - 388) + "px"

}

function closeTab() {
  var i;
  var close_one = false;
  var x = document.getElementsByClassName("tab");
  for (i = 0; i < x.length; i++) {
    if (x[i].style.display == "block") {
      x[i].style.display = "none";
      close_one = true;
    }
  }

  if (close_one) {
    document.getElementById("stage").style.height = (document.getElementById('stage').offsetHeight + 388) + "px"
  }
}

function about() {
  obj = "             graph.io (v.0.0.5)\n\nhttp://gitlab.com/rrleme/graph.io";

  alert(obj);

}
