/////////
// rgn();
// input:  	(int)  	n :: level
//			(int) 	posx :: position x in grid
//			(int)	posy :: position y in grid
//			(int)	facx :: distance x between node
// output: 	Rieger-Nishmirua Lattice (up to n)
/////////


var board = null;
var edges = null;
var grid = null;
var nodes = null;

function rgn(n, startx, starty, facx) {
  for (var i = 0; i < n; i++) {
 	if (i <= grid[i].length) {
  		if (i >= 1) {
      		board.add(grid[startx][starty+i]);
      		board.add(grid[startx+facx][starty+i]);
  		}
    	else {
      		board.add(grid[startx][starty+i]);
    	}

      	if (i >= 2) {
        	var parent = nodes.length-3;
          	var child = parent + 1;
          	edges.push([nodes[parent-1], nodes[child]]);
          	edges.push([nodes[parent], nodes[child+1]]);

          	if (i % 2 == 0) {
        		parent = nodes.length-3;
          		child = parent + 1;
          		edges.push([nodes[parent], nodes[child]]);
            }
          	else {
        		parent = nodes.length - 4;
          		child = nodes.length - 1;
          		edges.push([nodes[parent], nodes[child]]);
            }
        }
    }
  }

  // set root node
  edges.push([nodes[0], nodes[1]]);
  edges.push([nodes[0], nodes[2]]);

}

function main(mboard, mgrid, medges, mnodes) {
  board = mboard;
  nodes = mnodes;
  edges = medges;
  grid = mgrid;
  rgn(20, 15, 1, 2);
}


export { main };
