import * as graph from './modules/graph.js';
// import * as rn from './modules/RiegerNishimuraLattice.js';

document.addEventListener("onload", graph.startBoard());
document.getElementById("b_edge").onclick = function() {graph.switchType_edge();}
document.getElementById("b_node").onclick = function() {graph.switchType_node();}
document.getElementById("b_reset").onclick = function() {graph.reset();}
document.getElementById("b_select").onclick = function() {graph.select();}
document.getElementById("b_gen").onclick = function() {graph.gen();}
document.getElementById("b_ideal").onclick = function() {graph.ideal();}
document.getElementById("b_delete").onclick = function() {graph.doDelete();}
document.getElementById("b_topo").onclick = function() {graph.getTopo();}
document.getElementById("b_z_in").onclick = function() {graph.zoom_in();}
document.getElementById("b_z_out").onclick = function() {graph.zoom_out();}

document.getElementById("openTab_script").onclick = function() {graph.openTab('Script');}
document.getElementById("openTab_log").onclick = function() {graph.openTab('Log');}
document.getElementById("openTab_errors").onclick = function() {graph.openTab('Errors');}
document.getElementById("closeTab").onclick = function() {graph.closeTab();}

document.getElementById("run").onclick = function() {graph.runScript();}
document.getElementById("about").onclick = function() {graph.about();}
